C_SOURCES = $(wildcard kernel/*.c drivers/*.c interrupts/*.c)
HEADERS = $(wildcard kernel/*.h drivers/*.h interrupts/*.h)

OBJ = ${C_SOURCES:.c=.o interrupts/interrupt.o}

CC = gcc
GDB = gdb

CFLAGS = -m32 -g -ffreestanding -c
LDFLAGS = -m elf_i386 --ignore-unresolved-symbol _GLOBAL_OFFSET_TABLE_

os-image.bin: boot/bootsect.bin kernel.bin
	cat $^ > os-image.bin
	rm -rf *.dis *.o *.elf kernel.bin
	rm -rf kernel/*.o boot/*.bin drivers/*.o boot/*.o interrupts/*.o

kernel.bin: boot/kernel_entry.o ${OBJ}
	ld ${LDFLAGS} -o $@ -Ttext 0x1000 $^ --oformat binary

%.o: %.c
	${CC} ${CFLAGS} $< -o $@

%.o: %.asm
	nasm $< -f elf -o $@

%.bin: %.asm
	nasm $< -f bin -o $@
