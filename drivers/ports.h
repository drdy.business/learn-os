#pragma once

#include "../include/types.h"

unsigned char port_byte_in (unsigned short port);
void port_byte_out (unsigned short port, unsigned char data);

unsigned short port_word_in (unsigned short port);
void port_word_out (unsigned short port, unsigned short data);





u8 in8 (u8 port);
void out8 (u8 port, u8 data);
void out8_slow (u8 port, u8 data);

u16 in16 (u8 port);
void out16 (u8 port, u16 data);

u32 in32 (u8 port);
void out32 (u8 port, u32 data);

