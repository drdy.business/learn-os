#include "ports.h"

unsigned char port_byte_in (unsigned short port) {
    unsigned char result;
    /* Inline assembler syntax
    * !! Notice how the source and destination registers are switched from NASM !!
    *
    * '"=a" (result)'; set '=' the C variable '(result)' to the value of register e'a'x
    * '"d" (port)': map the C variable '(port)' into e'd'x register
    *
    * Inputs and outputs are separated by colons
    */
    __asm__("in %%dx, %%al" : "=a" (result) : "d" (port));
    return result;
}

void port_byte_out (unsigned short port, unsigned char data) {
    /* Notice how here both registers are mapped to C variables and
    * nothing is returned, thus, no equals '=' in the asm syntax 
    * However we see a comma since there are two variables in the input area
    * and none in the 'return' area
    */
    __asm__("out %%al, %%dx" : : "a" (data), "d" (port));
}

unsigned short port_word_in (unsigned short port) {
    unsigned short result;
    __asm__("in %%dx, %%ax" : "=a" (result) : "d" (port));
    return result;
}

void port_word_out (unsigned short port, unsigned short data) {
    __asm__("out %%ax, %%dx" : : "a" (data), "d" (port));
}



u8 in8 (u8 port) {
    u8 res;
    __asm__("in %%dx, %%al" : "=a" (res) : "d" (port));
    return res;
}

void out8 (u8 port, u8 data) {
    __asm__("out %%al, %%dx" : : "a" (data), "d" (port));
}

u16 in16 (u8 port) {
    u16 res;
    __asm__("in %%dx, %%ax" : "=a" (res) : "d" (port));
    return res;
}

void out16 (u8 port, u16 data) {
    __asm__("out %%ax, %%dx" : : "a" (data), "d" (port));
}


u32 in32 (u8 port) {
    u32 res;
    __asm__("in %%edx, %%eax" : "=a" (res) : "d" (port));
    return res;
}

void out32 (u8 port, u32 data) {
    __asm__("out %%eax, %%edx" : : "a" (data), "d" (port));
}

void out8_slow (u8 port, u8 data) {
    __asm__("out %%al, %%dx\njmp 1f\n1: jmp 1f\n1:" : : "a" (data), "d" (port));
}
