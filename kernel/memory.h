#pragma once

    /*
    Copy block of memory
        Copies the values of num bytes from the location pointed to by
        source directly to the memory block pointed to by destination.
    */
    void memcpy(void* src, void* dst, int nBytes);
