#pragma once

#include "../include/types.h"

typedef struct {
    uint16_t limit_low;
    uint16_t base_low;
    uint8_t base_high;
    uint8_t type;
    uint8_t flags_limit_high;
    uint8_t base_vhigh;
} GDTSegmentDescriptor_t;

typedef struct {
    GDTSegmentDescriptor_t nullSegmentSelector;
    GDTSegmentDescriptor_t unusedSegmentSelector;
    GDTSegmentDescriptor_t codeSegmentSelector;
    GDTSegmentDescriptor_t dataSegmentSelector;
} GDT_t;

void init_GDTSegmentDescriptor(GDTSegmentDescriptor_t* descriptor, u32 base, u32 limit, u8 flags);
u32 GDTSegmentDescriptor_Base(GDTSegmentDescriptor_t* descriptor);
u32 GDTSegmentDescriptor_Limit(GDTSegmentDescriptor_t* descriptor);

void init_GDT(GDT_t* gdt);
u16 GDT_CodeSegmentSelector(GDT_t* gdt);
u16 DataSegmentSelector(GDT_t* gdt);