#include "utils.h"

char* itoa(int value, char* str, int base) {
    int i = 0;
    char isNegative = 0;
 
    if (value == 0) {
        str[i++] = '0';
        str[i] = 0;
        return str;
    }
 
    if (value < 0 && base == 10) {
        isNegative = 1;
        value = -value;
    }
 
    // Getting digits
    while (value != 0) {
        int rem = value % base;
        str[i++] = (rem > 9) ? (rem-10) + 'a' : rem + '0';
        value = value / base;
    }
 
    // Applying minus
    if (isNegative)
        str[i++] = '-';
 
    // String terminator
    str[i] = 0;
 
    // Reversing the string
    reverse(str, i);
 
    return str;
}

void reverse(char* str, unsigned int length) {
    int start = 0;
    int end = length - 1;
    while (start < end) {
        swap((str+start++), (str+end--));
    }
}

void swap(char* a, char* b) {
    char tmp = *a;
    *a = *b;
    *b = tmp;
}