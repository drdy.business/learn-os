#include "memory.h"

void memcpy(void* src, void* dst, int nBytes) {
    if (nBytes <= 0) return;
    
    for (int i = 0; i < nBytes; ++i) {
        *((char*)dst + i) = *((char*)src + i);
    }
}