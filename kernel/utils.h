#pragma once

    /*
    Convert integer to string
        Converts an integer `value` to a null-terminated string
        using the specified base and stores the result in the 
        array given by str parameter.
    */
    char* itoa(int value, char* str, int base); 

    void reverse(char* str, unsigned int length);

    // Swaps two values using pointers
    void swap(char* a, char* b);