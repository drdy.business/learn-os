#include "../include/types.h"
#include "../drivers/screen.h"
#include "gdt.h"
#include "../interrupts/isr.h"
#include "../drivers/keyboard.h"
#include "utils.h"

void _start() {
    // GDT_t gdt;
    // InterruptManager_t ints;
    // clear_screen();
    // print code segment of gdt
    // init_GDT(&gdt);
    // PRINT("Initialized GDT...\n")

    // i8 m[100] = { 0 };

    // itoa(GDT_CodeSegmentSelector(&gdt), m, 10);

    // kprint(m);

    // init_InterruptManager(&ints, &gdt);
    // PRINT("Initialized IDT...\n")
    isr_install();
    init_keyboard();
    PRINT("Initialized stuff...\n")

    
    asm("sti");
    // activateInterrupts();

    // handleInterrupt


    PRINT("\n32bit protected mode... hello kernel\n");
}