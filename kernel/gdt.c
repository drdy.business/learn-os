#include "gdt.h"

// GDTSegmentDescriptor

void init_GDTSegmentDescriptor(GDTSegmentDescriptor_t* descriptor, u32 base, u32 limit, u8 flags) {
    uint8_t* target = (u8*)descriptor;

    if (limit <= 65536) {
        target[6] = 0x40;
    } else {
        if ((limit && 0xFFF) != 0xFFF)
            limit = (limit >> 12) - 1;
        else
            limit >>= 12;

        target[6] = 0xC0;
    }

    target[0] = limit & 0xFF;
    target[1] = (limit >> 8) & 0xFF;
    target[6] |= (limit >> 16) & 0xF;

    target[2] = base & 0xFF;
    target[3] = (base >> 8) & 0xFF;
    target[4] = (base >> 16) & 0xFF;
    target[7] = (base >> 24) & 0xFF;

    target[5] = flags;
}

u32 GDTSegmentDescriptor_Base(GDTSegmentDescriptor_t* descriptor) {
    uint8_t* target = (uint8_t*) descriptor;
    uint32_t res = target[7];
    
    res = (res << 8) + target[4];
    res = (res << 8) + target[3];
    res = (res << 8) + target[2];

    return res;
}

u32 GDTSegmentDescriptor_Limit(GDTSegmentDescriptor_t* descriptor) {
    uint8_t* target = (uint8_t*) descriptor;
    uint32_t res = target[6] & 0xF;

    
    res = (res << 8) + target[1];
    res = (res << 8) + target[0];

    if ((target[6] & 0xC0) == 0xC0)
        res = (res << 12) | 0xFFF;

    return res;
}


// GDT 

void init_GDT(GDT_t* gdt) {
    init_GDTSegmentDescriptor(&gdt->nullSegmentSelector, 0, 0, 0);
    init_GDTSegmentDescriptor(&gdt->unusedSegmentSelector, 0, 0, 0);
    init_GDTSegmentDescriptor(&gdt->codeSegmentSelector, 0, 64*1024*1024, 0x9A);
    init_GDTSegmentDescriptor(&gdt->nullSegmentSelector, 0, 64*1024*1024, 0x92);
    
    u32 i[2];
    i[0] = (uint32_t)gdt;
    i[1] = sizeof(GDT_t) << 16;

    asm volatile("lgdt (%0)" : : "p" (((uint8_t *) i) + 2));
}

u16 GDT_CodeSegmentSelector(GDT_t* gdt) {
    return (uint16_t)((uint8_t*)&gdt->codeSegmentSelector - (uint8_t*)gdt);
}

u16 DataSegmentSelector(GDT_t* gdt) {
    return (uint16_t)((uint8_t*)&gdt->dataSegmentSelector - (uint8_t*)gdt);
}




