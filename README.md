# S-OS
## LINUX:

Need these:
- gcc
- nasm
- make

### Run this
```cmd
> make
```

You can clean the directories by running
```cmd
> ./clean
```

Then if you want to run it in windows, copy the os-image.bin file and run this command in cmd
```cmd
> run
```