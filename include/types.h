#pragma once

typedef char i8;
typedef unsigned char u8;

typedef i8 int8_t;
typedef u8 uint8_t;

typedef short i16;
typedef unsigned short u16;

typedef i16 int16_t;
typedef u16 uint16_t;

typedef int i32;
typedef unsigned int u32;

typedef i32 int32_t;
typedef u32 uint32_t;

typedef long long i64;
typedef unsigned long long u64;

typedef i64 int64_t;
typedef u64 uint64_t;

typedef float f32;
typedef double f64;
