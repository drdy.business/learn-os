#include "idt.h"

void set_idt_entry(u8 entry, u32 handler) {
    idt[entry].isr_low      = handler & 0xFFFF;
    idt[entry].kernel_cs    = KERNEL_CS;
    idt[entry].reserved     = 0;
    idt[entry].attributes   = INT_GATE_FLAGS;
    idt[entry].isr_high     = handler >> 16;
}

void init_idt() {
    idtr.limit  = IDT_ENTRIES * sizeof(idt_entry_t) - 1;
    idtr.base   = (u32) idt;

    __asm__ __volatile__ ("lidtl (%0)" : : "r" (&idtr)); 
}