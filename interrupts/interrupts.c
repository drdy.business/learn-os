// #include "interrupts.h"

// #include "../drivers/screen.h"

// void setIDTEntry(InterruptManager_t* intManager,
//                  u8 intNum, 
//                  u16 codeSegmentSelectorOffset, 
//                  void (*handler)(), 
//                  u8 DescriptorPrivilegeLevel, 
//                  u8 type
// ) {

//     const u8 IDT_DESC = 0x80;

//     intManager->IDT[intNum].handlerAddressLowBits = ((u32)handler) & 0xFFFF; 
//     intManager->IDT[intNum].handlerAddressHighBits = (((u32)handler) >> 16) & 0xFFFF; 
//     intManager->IDT[intNum].gdt_codeSegmentSelector = codeSegmentSelectorOffset;
//     intManager->IDT[intNum].access = IDT_DESC | type | ((DescriptorPrivilegeLevel & 3) << 5);
//     intManager->IDT[intNum].reserved = 0;
// }

// void init_InterruptManager(InterruptManager_t* intManager, GDT_t* gdt) {
//     u16 CodeSegment = GDT_CodeSegmentSelector(gdt);
//     const u8 IDT_INTERRUPT_GATE = 0xE;

//     for (u16 i = 0; i < 256; ++i) {
//         setIDTEntry(intManager, i, CodeSegment, &handleInterruptRequest0x00, 0, IDT_INTERRUPT_GATE);
//     }

//     setIDTEntry(intManager, 0x20, CodeSegment, &handleInterruptRequest0x00, 0, IDT_INTERRUPT_GATE);
//     setIDTEntry(intManager, 0x21, CodeSegment, &handleInterruptRequest0x01, 0, IDT_INTERRUPT_GATE);
//     setIDTEntry(intManager, 0x28, CodeSegment, &handleInterruptRequest0x08, 0, IDT_INTERRUPT_GATE);

//     out8_slow(0x20, 0x11); // PIC master command
//     out8_slow(0x21, 0x11); // PIC slave  command

//     out8_slow(0xA0, 0x20); // PIC master data
//     out8_slow(0xA1, 0x28); // PIC slave  data

//     out8_slow(0xA0, 0x04); // PIC master
//     out8_slow(0xA1, 0x02); // PIC slave
    
//     out8_slow(0xA0, 0x01); // PIC master
//     out8_slow(0xA1, 0x01); // PIC slave

//     out8_slow(0xA0, 0x00); // PIC master
//     out8_slow(0xA1, 0x00); // PIC slave


//     IDTPointer_t idtp;
//     idtp.size = 256 * sizeof(GateDescriptor_t) - 1;
//     idtp.base = (u32)intManager->IDT;

//     asm volatile("lidt %0" : : "m" (idtp));
// }

// void activateInterrupts() {
//     asm("sti");
// }

// extern u32 handleInterrupt(u8 intNum, u32 esp) {
//     PRINT("INTERRUPT")

//     return esp;
// }

// void handleInterruptRequest0x00() {
//     PRINT("INTERRUPT 0x00")
//     asm("cli");
// }