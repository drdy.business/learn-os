#pragma once
#include "../include/types.h"

// P = 1, DPL = 00, S = 0, Type = 1110 (32bit int  gate)
#define INT_GATE_FLAGS  0x8E

#define KERNEL_CS 8
#define IDT_ENTRIES 256

// IDT entry
typedef struct {
	u16    isr_low;      // The lower 16 bits of the ISR's address
	u16    kernel_cs;    // The GDT segment selector that the CPU will load into CS before calling the ISR
	u8     reserved;     // Set to zero
	u8     attributes;   // Type and attributes; see the IDT page
	u16    isr_high;     // The higher 16 bits of the ISR's address
} __attribute__((packed)) idt_entry_t;

// IDT register
typedef struct {
	u16	limit;
	u32	base;
} __attribute__((packed)) idtr_t;

static idt_entry_t idt[IDT_ENTRIES] = { 0 };    // Actual IDT
static idtr_t idtr;                             // IDT register instance

// Add an ISR to the IDT
void set_idt_entry(u8 entry, u32 handler);
void init_idt();