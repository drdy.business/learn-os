#include "isr.h"
#include "idt.h"
#include "../drivers/screen.h"
#include "../drivers/ports.h"
#include "../kernel/utils.h"

isr_t interrupt_handlers[256];

/* Can't do this with a loop because we need the address
 * of the function names */
void isr_install() {
    set_idt_entry(0, (u32)isr0);
    set_idt_entry(1, (u32)isr1);
    set_idt_entry(2, (u32)isr2);
    set_idt_entry(3, (u32)isr3);
    set_idt_entry(4, (u32)isr4);
    set_idt_entry(5, (u32)isr5);
    set_idt_entry(6, (u32)isr6);
    set_idt_entry(7, (u32)isr7);
    set_idt_entry(8, (u32)isr8);
    set_idt_entry(9, (u32)isr9);
    set_idt_entry(10, (u32)isr10);
    set_idt_entry(11, (u32)isr11);
    set_idt_entry(12, (u32)isr12);
    set_idt_entry(13, (u32)isr13);
    set_idt_entry(14, (u32)isr14);
    set_idt_entry(15, (u32)isr15);
    set_idt_entry(16, (u32)isr16);
    set_idt_entry(17, (u32)isr17);
    set_idt_entry(18, (u32)isr18);
    set_idt_entry(19, (u32)isr19);
    set_idt_entry(20, (u32)isr20);
    set_idt_entry(21, (u32)isr21);
    set_idt_entry(22, (u32)isr22);
    set_idt_entry(23, (u32)isr23);
    set_idt_entry(24, (u32)isr24);
    set_idt_entry(25, (u32)isr25);
    set_idt_entry(26, (u32)isr26);
    set_idt_entry(27, (u32)isr27);
    set_idt_entry(28, (u32)isr28);
    set_idt_entry(29, (u32)isr29);
    set_idt_entry(30, (u32)isr30);
    set_idt_entry(31, (u32)isr31);

    // Remap the PIC
    port_byte_out(0x20, 0x11);
    port_byte_out(0xA0, 0x11);
    port_byte_out(0x21, 0x20);
    port_byte_out(0xA1, 0x28);
    port_byte_out(0x21, 0x04);
    port_byte_out(0xA1, 0x02);
    port_byte_out(0x21, 0x01);
    port_byte_out(0xA1, 0x01);
    port_byte_out(0x21, 0x0);
    port_byte_out(0xA1, 0x0); 

    // Install the IRQs
    set_idt_entry(32, (u32)irq0);
    set_idt_entry(33, (u32)irq1);
    set_idt_entry(34, (u32)irq2);
    set_idt_entry(35, (u32)irq3);
    set_idt_entry(36, (u32)irq4);
    set_idt_entry(37, (u32)irq5);
    set_idt_entry(38, (u32)irq6);
    set_idt_entry(39, (u32)irq7);
    set_idt_entry(40, (u32)irq8);
    set_idt_entry(41, (u32)irq9);
    set_idt_entry(42, (u32)irq10);
    set_idt_entry(43, (u32)irq11);
    set_idt_entry(44, (u32)irq12);
    set_idt_entry(45, (u32)irq13);
    set_idt_entry(46, (u32)irq14);
    set_idt_entry(47, (u32)irq15);

    init_idt();
}

/* To print the message which defines every exception */
const char *exception_messages[] = {
    "Division By Zero",
    "Debug",
    "Non Maskable Interrupt",
    "Breakpoint",
    "Into Detected Overflow",
    "Out of Bounds",
    "Invalid Opcode",
    "No Coprocessor",

    "Double Fault",
    "Coprocessor Segment Overrun",
    "Bad TSS",
    "Segment Not Present",
    "Stack Fault",
    "General Protection Fault",
    "Page Fault",
    "Unknown Interrupt",

    "Coprocessor Fault",
    "Alignment Check",
    "Machine Check",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",

    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved"
};

void isr_handler(registers_t r) {
    PRINT("received interrupt: ")
    char s[3];
    itoa(r.int_no, s, 10);
    kprint(s);
    kprint(exception_messages[r.int_no]);
}

void register_interrupt_handler(u8 n, isr_t handler) {
    interrupt_handlers[n] = handler;
}

void irq_handler(registers_t r) {
    /* After every interrupt we need to send an EOI to the PICs
     * or they will not send another interrupt again */
    if (r.int_no >= 40) port_byte_out(0xA0, 0x20); /* slave */
    port_byte_out(0x20, 0x20); /* master */

    /* Handle the interrupt in a more modular way */
    if (interrupt_handlers[r.int_no] != 0) {
        isr_t handler = interrupt_handlers[r.int_no];
        handler(r);
    }
}